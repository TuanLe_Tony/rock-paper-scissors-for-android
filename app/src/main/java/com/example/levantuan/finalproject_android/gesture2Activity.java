package com.example.levantuan.finalproject_android;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.seismic.ShakeDetector;

import java.util.Random;

public class gesture2Activity extends AppCompatActivity implements ShakeDetector.Listener {

    ImageView iv2;
    int timer = 3;
    String currentChoice;
    public static final String PREFERENCES_NAME = "RockPaperScissor";
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gesture2);
        prefs = getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);

        SensorManager manager = (SensorManager) getSystemService(SENSOR_SERVICE);
        ShakeDetector detector = new ShakeDetector(this);
        detector.start(manager);


        iv2 = (ImageView) findViewById(R.id.iv2);
        Toast.makeText(this, "second gesture class", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hearShake() {

        //2. this function AUTOAGICALL gets called
        //every time you shake the phone

        //print out a message when the phone shakes

        try {

            if (timer == 3) {
                timer = timer - 1;

                Toast.makeText(this, "Shaking 1 time", Toast.LENGTH_SHORT).show();


            } else if (timer == 2) {
                timer = timer - 1;
                Toast.makeText(this, "Shaking 2 times", Toast.LENGTH_SHORT).show();


            } else if (timer == 1) {
                timer = timer - 1;

            } else if (timer == 0) {
                timer = timer - 1;

                Random r = new Random();
                int randomchoice = r.nextInt(3);


                //first cpu choice....
                if (randomchoice == 0) {
                    currentChoice = "rock";
                    iv2.setImageResource(R.drawable.rock);

                } else if (randomchoice == 1) {
                    currentChoice = "paper";
                    iv2.setImageResource(R.drawable.paper);

                } else if (randomchoice == 2) {
                    currentChoice = "scissor";
                    iv2.setImageResource(R.drawable.scissor);
                }
                //update current choice to firebase....
                DatabaseReference zonesRef = FirebaseDatabase.getInstance().getReference("CurrentGame");
                DatabaseReference zone1Ref = zonesRef.child(prefs.getString("random", "")).child(prefs.getString("GameKey", "")).child("choice");
                zone1Ref.setValue(currentChoice);
                Log.d("successfully!!!", "upated values!!");


            }

//


        } catch (Exception e) {
            Log.d("sakhi", "Error while turning on camera flash");
            Toast.makeText(this, "Error while turning on camera flash", Toast.LENGTH_SHORT).show();

        }

    }
}

