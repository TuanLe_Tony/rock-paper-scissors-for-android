
# ROCK - PAPER - SCISSORS
## Shaking the phone to random pick rock - paper - scissors. 
Sample code for a ROCK - PAPER - SCISSORS app. Written in java + Firebase + API for google map.

## Software Requirements - Scenario 

###  Getting players's location near by.
###  3 People play with each other (interact)



# SIGN IN 

![Scheme](Android-Images/signin.png)

Created a account.

# Menu options
![Scheme](Android-Images/home.jpg)

## Select Nearby Your Location menu.
# Showing your location and other players's locaiton.
![Scheme](Android-Images/location.jpg)

# GAMEPLAY 
![Scheme](Android-Images/draw.jpg)








